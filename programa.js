
let fuenteMapas=new ol.source.OSM()

let configTeselas= {
    source: fuenteMapas,
}

let teselas= new ol.layer.Tile(configTeselas);


var map = new ol.Map({
    target: 'map',

    layers: [
        teselas
    ],
    view: new ol.View({
        center: ol.proj.fromLonLat([-74.0721, 4.7110]),
        zoom: 13
    })
});


function mostrarMarcador() {
    
    //Crear los objetos que referencian a los cuadros de texto
    let cuadroLatitud = document.getElementById("latitud");
    let cuadroLongitud = document.getElementById("longitud");

    //Extraer los valores de los cuadros de texto
    let latitud = cuadroLatitud.value;
    let longitud = cuadroLongitud.value;

    //Crear un objeto con las coordenadas reproyectadas 
    let coordenadasProyectadas = ol.proj.fromLonLat([longitud, latitud]);

    //Crear un objeto de geometría tipo punto
    let punto = new ol.geom.Point(coordenadasProyectadas);

    //Crear un objeto con los datos para crear un marcador

    let configMarker = {
        geometry: punto,
    };

    //Crear un objeto tipo Feature
    var marker = new ol.Feature(configMarker);


    //Crear un objeto con los datos para definir una fuente de datos vectoriales
    var configVectorSource = {
        features: [marker]
    }

    //Crear un objeto de fuente de datos vectoriales
    var vectorSource = new ol.source.Vector(configVectorSource);

    //Crear un objeto con los datos para crear una capa vectorial en el mapa

    var configLayer = {
        source: vectorSource,
    }

    //Crear un objeto de capa vectorial (Vector)

    var markerVectorLayer = new ol.layer.Vector(configLayer);

    //Crear un objeto con los datos para crear un ícono
    var configIcono = {
        scale: 1,
        anchor: [0.5, 1],
        src: "https://openlayers.org/en/latest/examples/data/icon.png",
        opacity: 1
    }

    //Crear un objeto tipo Icon
    var icono = new ol.style.Icon(configIcono);

    //Crear un objeto con los datos para crear un estilo

    var configIconStyle={
        image: icono
    }

    //Crear un objeto de tipo Style

    var iconStyle = new ol.style.Style(configIconStyle);

    //Agregar el estilo al marcador
    marker.setStyle(iconStyle);

    //Agregar la capa vectorial al mapa

    map.addLayer(markerVectorLayer);


}